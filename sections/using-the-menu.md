## Using the menu

Select “Menu” on the rotary switch and navigate using the function buttons:

![](/images/up-arrow-button.png) = Scroll Up ![](/images/down-arrow-button.png) = Scroll Down ![](/images/return-button.png) = Enter

**NOTE:  To EXIT the MENU at any time simply move the rotary switch to any position other than `Menu`.  Any changes that have not been “entered” will be ignored.**

![](/images/5-menu.png)

As you scroll up or down the side LEDs illuminate to point to the active line

| MENU  | SUB MENU | OPTIONS/COMMENTS                                         |
| ----  | ----     | ----                                                     |
| SETUP | Language | **English**                                              |
|       | SET TIME | HH:MM:SS format<br>e.g. 7 am  = 07:00:00, 7pm = 19:00:00 |
|       | SET DATE | DD/MM/YY format                                          |
|       | PRINTER  | KM IRP<br>KANE IRP-2<br>WIRELESS<br>SERIAL               |
|       | PASSKEY  | 1111 (wait 5 secs after entering last digit)

As you scroll up or down the side LEDs illuminate to point to the active line

**NOTE:  To EXIT the MENU at any time simply move the rotary switch to any position other than `Menu`.  Any changes that have not been “entered” will be ignored.**

| MENU  | SUB MENU          | OPTIONS/COMMENTS                                                                                             |
| ----  | ----              | ----                                                                                                         |
| UNITS | Fuel Type         | NAT GAS, TOWN GAS, COKE GAS, PROPANE, BUTANE, LPG, LIGHT OIL, BIO OIL, WOOD PELLETS, BIO GAS, NAT GAS 1 to 5 |
|       | Fuel Origin       | UK, FRANCE, SPAIN, N AMERICA, BELGIUM, NETHERLAND                                                            |
|       | EFFICIENCY        | GROSS, NET, GROSS COND, NET COND                                                                             |
|       | PRESSURE          | See next table below                                                                                         |
|       | GAS               | ppm, ppm(n), mg/m3, mg/m3(n), mg/kWh, mg/kWh(n)                                                              |
|       | TEMP              | C , F                                                                                                        |
|       | O<sub>2</sub> REF | Up/down to set value (3% default)                                                                            |
|       | NOx CALC          | Up/down to set value (5% default)                                                                            |
|       | BACK              | &nbsp;                                                                                                       |

As you scroll up or down the side LEDs illuminate to point to the active line

**NOTE:  To EXIT the MENU at any time simply move the rotary switch to any position other than `Menu`.  Any changes that have not been “entered” will be ignored.**

|  MENU | SUB MENU   | OPTIONS/COMMENTS                                                                                                                                |
| ----      | ----       | ----                                                                                                                                            |
| PRESSURE  | FILTER     | OFF = norman response<br>ON = slower (damped) response                                                                                          |
|           | RESOLUTION | LOW = e.g. 0.01mbar resolution<br>HIGH = displays to an extra decimal place                                                                     |
|           | UNITS      | mbar, Pa, PSI, mmHg, hPa, inH<sub>2</sub>O, mmH2O, kPa, psi                                                                                     |
|           | TIME       | LET BY = Set duration of let-by test in minutes, Default = 1 minute<br>STABIL'N = Set duration of stabilisation in minutes.  Default = 1 minute<br>TIGHTN’S = Set duration of tightness test in minutes.  Default = 2 minute |
|           | BACK              | &nbsp;                                                                                                       |

As you scroll up or down the side LEDs illuminate to point to the active line

**NOTE:  To EXIT the MENU at any time simply move the rotary switch to any position other than `Menu`.  Any changes that have not been “entered” will be ignored.**

| MENU   | SUB MENU  | OPTIONS/COMMENTS                                                                                                    |
| ----   | ----      | ----                                                                                                                |
| SCREEN | CONTRAST  | Factory setting is 14                                                                                               |
|        | BACKLIGHT | 0 to 300 secs                                                                                                       |
|        | AUX       | Enables users to customise the parameters on the AUX display:  LINE 1, LINE 2, LINE 3, LINE 4, LINE 5, LINE 6, BACK |
|        | BACK      | &nbsp;                                                                                                              |

As you scroll up or down the side LEDs illuminate to point to the active line

**NOTE:  To EXIT the MENU at any time simply move the rotary switch to any position other than `Menu`.  Any changes that have not been “entered” will be ignored.**

| MENU   | SUB MENU   | OPTIONS/COMMENTS                            |
| ----   | ----       | ----                                        |
| REPORT | AUX        | Stored AUX tests<br>VIEW, DEL ALL, BACK     |
|        | COMBUSTION | Stored combustion tests:<br>VIEW, ALL, BACK |
|        | COMMISSION | Stored commission tests:<br>VIEW, ALL, BACK |
|        | PRS/TEMP   | Stored pressure tests:<br>VIEW, ALL, BACK   |
|        | TIGHTN'S   | Stored tightness tests:<br>VIEW, ALL, BACK  |
|        | ROOM CO    | Stored room CO tests:<br>VIEW, ALL, BACK    |
|        | HEADER     | LINE 1<br>LINE 2<br>BACK                    |
|        | BACK       | &nbsp;                                      |

As you scroll up or down the side LEDs illuminate to point to the active line

**NOTE:  To EXIT the MENU at any time simply move the rotary switch to any position other than `Menu`.  Any changes that have not been “entered” will be ignored.**

| MENU    | SUB MENU | OPTIONS/COMMENTS                                                            |
| ----    | ----     | ----                                                                        |
| SERVICE | CODE     | Password protected for authorised service agents only. Leave set to 000000. |

As you scroll up or down the side LEDs illuminate to point to the active line

**NOTE:  To EXIT the MENU at any time simply move the rotary switch to any position other than `Menu`.  Any changes that have not been “entered” will be ignored.**
