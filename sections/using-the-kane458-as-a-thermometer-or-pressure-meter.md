## USING THE KANE458 AS A THERMOMETER OR PRESSURE METER

Rotate the dial to the PRS/TEMP position.

The display will show:

![](/images/6-pressure-thermometer.png)

The standard printout for this mode is as follows:

![](/images/6-pressure-thermometer-printout.png)

_**If using larger bore tubing when performing pressure tests:**_

Push ‘orange’ tube over the rim of the spigot to ensure a gas tight seal.
![](/images/4-3-correct-pressure-fitting.png)


This may not produce a gas tight seal.
![](/images/4-3-incorrect-pressure-fitting.png)

