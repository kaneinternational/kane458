## ANALYSER PROBLEM SOLVING

If any problems are not solved with these solutions, contact us or an authorised repair center.

| Fault symptom                                                          | Causes / Solutions                                                                                     |
| :----                                                                  | :----                                                                                                  |
| Oxygen too high<br>CO<sub>2</sub> too low                              | Air leaking into probe, tubing, water trap, connectors or internal to analyser.                        |
| Batteries not holding charge<br>Analyser not running on mains adapter. | Batteries exhausted.<br>AC charger not giving correct output.<br>No fuse                               |
| Analyser does not respond to flue gas                                  | Particle filter blocked.<br>Probe or tubing blocked.<br>Pump not working or damaged with contaminants. |
| Net temperature or Efficiency calculation incorrect.                   | Ambient temperature set wrong during Automatic Calibration.                                            |
| Analyser does not respond to flue gas                                  | Particle filter blocked.<br>Probe or tubing blocked.<br>Pump not working or damaged with contaminants. |
| Net temperature or Efficiency calculation incorrect.                   | Ambient temperature set wrong during Automatic Calibration.                                            |
| Flue temperature readings erratic                                      | Temperature plug reversed in socket.<br>Faulty connection or break in cable or plug.                   |
| T flue or ΔT displays  `(-N/F-)`                                       | Probe not connected.<br>Faulty connection or break in cable or plug.                                   |
| EFF or  X-Air displays  `(- O2++-)`                                    | CO<sub>2</sub> reading is below 2%.<br>0<sub>2</sub> > 18%                                             |
| Analyser just continually beeps                                        | Turn dial back to MENU and press ENTER<br>Turn dial back to Tightness and press ENTER                  |
