## KANE458 OVERVIEW

The KANE458 is based on the KANE456 with the addition of a dilution pump which helps to protect the CO sensor from high concentration that otherwise might damage the sensor.

The KANE458 Combustion Analyser measures carbon dioxide (CO<sub>2</sub>), carbon monoxide (CO), differential temperature and differential pressure.  

The direct measurement of CO<sub>2</sub> is achieved using a Kane designed infra-red sensing system. Below 1% CO<sub>2</sub> the readings of CO<sub>2</sub> are displayed to two decimal places.

CO<sub>2</sub> is set to zero in fresh air automatically after the initial countdown. The countdown varies between 90 and 60 seconds dependent on ambient temperature.

If “RESET GAS ZERO” is indicated ensure that the unit is in **outside fresh air** before pressing the button with an “Enter” symbol.

It is important that re-zeroing is done in **outside fresh** air as indoor CO<sub>2</sub> levels are affected by human breath.

It calculates oxygen (O<sub>2</sub>), CO/CO<sub>2</sub> ratio, losses, combustion efficiency (Net  or Condensing Gross).

The KANE458 Combustion Analyser can also measure CO levels in ambient air - useful when a CO Alarm is triggered.  It can also perform a Room CO Test for up to 30 minutes duration.   

A structured Commissioning Test has been included for the installation of boilers.

The analyser has a protective rubber cover with a magnet for “hands–free” operation and is supplied with a flue probe with integral temperature sensor.   

A low flow detection system warns of low flow and switches the pump off. This also helps to prevent water ingress from overfilled water traps. Its LCD display is protected with a toughened screen.

The large display shows 6 readings at a time and all data can be printed via an optional infrared printer.  The printed data can be 'live' data or ‘stored’ data.   

The memory can store up to:

- 60 combustion tests
- 20 AUX tests
- 20 let-by/tightness tests
- 20 temperature & pressure tests
- 20 room CO tests
- 20 commissioning Tests

Two lines of 20 characters can be added to the header of printouts. Printouts can be made on the optional Kane IRP printers with ‘fast print’ capability using the IRP2 printer.  Alternatively the analyser can be equipped with optional wireless communications to either Android or Apple devices.

The analyser is controlled using 4 function buttons and a rotary dial.   

The four buttons (from left to right) switch on and off the analyser, switch on and off the torch light, switch on and off the pump and send data to a printer or to the memory.  The buttons with UP, DOWN and ENTER arrows also change settings such as date, time, fuel, etc. when in MENU mode.
