## BATTERY

### Battery Type

This analyser has been designed for use with disposable alkaline batteries or rechargeable Nickel Metal Hydride (NiMH) batteries.  No other battery types are recommended.

![](/images/warning-sign.png) **WARNING**
The battery charger unit must only be used when NiMH batteries are fitted. Do not mix NiMH cells of different capacities or from different manufacturers. All four cells must be identical.

### Replacing Batteries
Turn over the analyser, remove its protective rubber cover and fit 4 “AA” batteries [available [HERE](https://www.kane.co.uk/shop/products/b15)] in the battery compartment. Take great care to ensure they are fitted with the correct battery polarity. Replace the battery cover and protective rubber cover.   

Switch the analyser on and check that the analyser’s time and date are correct. To reset see [USING THE MENU](/sections/using-the-menu.html#using-the-menu), Section 5.   

### Charging NiMH Batteries

Ensure that you use the correct charger. The part number is **19278** [available [HERE](https://www.kane.co.uk/shop/products/19278-charger-adaptor-100-240v)].

To fully charge NiMH batteries:
- The charger must be connected and switched on.
- When charging, the red Battery Charging Indicator will illuminate.
- After a few seconds, the display will show “ CHARGING BATTERY” if they
need extra charge. If they are already fully charged this message will not
appear.

The first charge should be for 12 hours continuously.  NiMH batteries are suitable for
top up charging at any time, even for short periods.

An in-vehicle charger can be used to top up the analyser's batteries from a 12 volt vehicle battery.  The part number is 18342. Battery Disposal Always dispose of depleted batteries using approved disposal methods that protect the environment.

### Battery Disposal

Always dispose of depleted batteries using approved disposal methods that protect the environment.
