<div class="cover">
  <h1>{{ book.product.name }}</h1>

  <h2>Flue Gas Analyser with direct CO<sub>2</sub> measurement and CO sensor protection</h2>


  <img src="https://kane-uk.s3.amazonaws.com/uploads/product_image/image/205/m_KANE458_unit_RGB.jpg" alt={{ book.product.name }} />
</div>

**Stock No:19902  **
May 2017  
&#169; Kane International  

